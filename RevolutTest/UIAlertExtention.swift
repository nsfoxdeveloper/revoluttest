import Foundation
import UIKit

class UIAlertExtention {
    
    func createAction(title: String?, message: String?, preferredStyle: UIAlertControllerStyle) -> UIAlertController {
        return UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
    }
    
    func addAction(title: String, style: UIAlertActionStyle, action: ((UIAlertAction) -> Void)?) -> UIAlertAction {
        return UIAlertAction(title: title, style: style, handler: action)
    }
    
    func show(viewController: UIViewController, alert: UIAlertController) {
        viewController.present(alert, animated: true, completion: nil)
    }
    
    func show(title: String, message: String, showOn viewController: UIViewController) {
        let action = UIAlertExtention()
        let act = action.createAction(title: title, message: message, preferredStyle: .alert)
        act.addAction(action.addAction(title: "ОК", style: .cancel, action: nil))
        action.show(viewController: viewController, alert: act)
    }
    
}
