//
//  CurrencyDataStorage.swift
//  RevolutTest
//
//  Created by Yuriy Durnev on 02.09.17.
//  Copyright © 2017 Revolut. All rights reserved.
//

import Foundation
import RealmSwift

protocol CurrencyProtocol {
    associatedtype T
    
    func getAll() -> Results<Currency>
    func getCurrencyBy(userCurrency: UserCurrency) -> Currency?
    func save<T: Object>(objects: [T])
}

class CurrencyDataStorage: CurrencyProtocol {
    typealias T = Results<Currency>
    let realm = try! Realm()
    
    func getAll() -> Results<Currency> {
        return realm.objects(Currency.self)
    }
    
    func getCurrencyBy(userCurrency: UserCurrency) -> Currency? {
        guard let name = userCurrency.name else {
            return nil
        }
        return realm.object(ofType: Currency.self, forPrimaryKey: name)
    }
    
    func save<T: Object>(objects: [T]) {
       try! realm.write {
            realm.add(objects, update: true)
        }
    }
}
