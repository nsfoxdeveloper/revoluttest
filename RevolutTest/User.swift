//
//  User.swift
//  RevolutTest
//
//  Created by Yuriy Durnev on 02.09.17.
//  Copyright © 2017 Revolut. All rights reserved.
//

import Foundation
import RealmSwift

class User: Object {
    @objc dynamic var id = 1
    @objc dynamic var name = "Test User"
    var currency = List<UserCurrency>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func createUser() {
        let realm = try! Realm()
        if let _ = realm.object(ofType: User.self, forPrimaryKey: 1) {
            return
        }
        try! realm.write {
            let user = User()
            user.currency = self.createDefaultValue()
            realm.add(user)
        }
        HistoryStorage().save(operationDescription: "Add default balance to user")
    }
    
    func createDefaultValue() -> (List<UserCurrency>) {
        var currencyName = ["USD", "EUR", "GBP"]
        var currencySymbol = ["$", "€", "£"]
        let defaultValue = List<UserCurrency>()
        for i in 0...2 {
            let currency = UserCurrency()
            currency.name = currencyName[i]
            currency.symbol = currencySymbol[i]
            defaultValue.append(currency)
            
        }
        return defaultValue
    }
    
    func getUser() -> User {
        let realm = try! Realm()
        return realm.object(ofType: User.self, forPrimaryKey: 1)!
    }
    
}

class UserCurrency: Object {
    @objc dynamic var name: String?
    @objc dynamic var symbol: String?
    @objc dynamic var value: Double = 100
    
    override static func primaryKey() -> String? {
        return "name"
    }
    
}
