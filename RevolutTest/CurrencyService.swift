//
//  CurrencyService.swift
//  RevolutTest
//
//  Created by Yuriy Durnev on 02.09.17.
//  Copyright © 2017 Revolut. All rights reserved.
//

import Foundation
import SWXMLHash

class CurrencyService: NSObject {
    
    var timer: Timer?
    
    func startUpdate() {
        self.timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(CurrencyService.sendRequest), userInfo: nil, repeats: true)
    }
    
    func stopTimer() {
        guard let timer = self.timer else {
            return
        }
        
        timer.invalidate()
    }
    
    @objc func sendRequest() {
        DispatchQueue.global(qos: .userInteractive).async {
            guard let url = URL(string: "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml") else { return }
            let request = URLRequest(url: url)
            let session = URLSession(configuration: URLSessionConfiguration.default)
            
            let task = session.dataTask(with: request) { [unowned self] (data, url, error) in
                if let error = error {
                    debugPrint(error.localizedDescription)
                    return
                }
                guard
                    let data = data,
                    let string = String(data: data, encoding: .utf8)
                    else {return}
                
                self.parse(data: string)
                session.finishTasksAndInvalidate()
            }
            
            task.resume()
        }
    }
    
    func parse(data: String) {
        let xml = SWXMLHash.parse(data)
        do {
            let currencys: [Currency] = try xml["gesmes:Envelope"]["Cube"]["Cube"]["Cube"].value()
            CurrencyDataStorage().save(objects: currencys)
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
   
}
