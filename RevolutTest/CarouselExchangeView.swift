//
//  CarouselExchangeView.swift
//  RevolutTest
//
//  Created by Yuriy Durnev on 03.09.17.
//  Copyright © 2017 Revolut. All rights reserved.
//

import UIKit
import iCarousel
import RealmSwift

class CarouselExchangeView: UIView, iCarouselDataSource, iCarouselDelegate {

    public var visibleCell: iExchangeViewCell?
    public var cellBlockVisible: ((_ visibleCell: iExchangeViewCell, _ indexPath: IndexPath) -> Void)?
   
    /*
     Был вариант использовать UICollectionView, но
     если у textInput постоянно вызывать becomeFirstResponder, то collection view начинает не кисло так колбисть
     а именно пропадает одна из ячейки, по-этому взять старую добрую либу + решает задачу кругового скролла
     **/
    @IBOutlet weak var carousel: iCarousel!
    @IBOutlet weak var pageControl: UIPageControl!
    public var user = User().getUser()
    private var notificationToken: NotificationToken? = nil
    
    override func awakeFromNib() {
        carousel.type = .linear
        carousel.bounces = true
        carousel.delegate = self
        carousel.dataSource = self
        carousel.isPagingEnabled = true
        carousel.ignorePerpendicularSwipes = true
        
        notificationToken = user.currency.addNotificationBlock { [weak self] (changes: RealmCollectionChange) in
            guard let carousel = self?.carousel else { return }
            switch changes {
            case .initial:
                carousel.reloadData()
                break
            case .update(_, _, _, _):
                carousel.reloadData()
                break
            case .error(let error):
                debugPrint(error.localizedDescription)
                break
            }
        }
    }
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        self.pageControl.numberOfPages = self.user.currency.count
        return self.user.currency.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        var cell: iExchangeViewCell?
        if let view = view as? iExchangeViewCell {
            cell = view
        } else {
            cell = iExchangeViewCell.fromNib()
            cell?.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 150)
            cell?.currency = self.user.currency[index]
        }
        
        return cell!
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        
        if option == .wrap {
            return 1.0
        }
        return value
    }
    
    func carouselDidEndScrollingAnimation(_ carousel: iCarousel) {
        guard let cell = carousel.currentItemView as? iExchangeViewCell else {
            return
        }
        cell.textInput.resignFirstResponder()
        self.visibleCell?.textInput.text = nil
        self.visibleCell = cell
        self.pageControl.currentPage = carousel.currentItemIndex
        cellBlockVisible?(cell, IndexPath(row: carousel.currentItemIndex, section: 0))
    }
    
    func nextItem() {
        var nextIndex: Int?
        if (carousel.currentItemIndex + 1) <= self.user.currency.count {
            nextIndex = carousel.currentItemIndex + 1
        } else {
            nextIndex = carousel.currentItemIndex - 1
        }
        self.carousel.scrollToItem(at: nextIndex!, animated: true)
    }
}
