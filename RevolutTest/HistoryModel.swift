//
//  HistoryModel.swift
//  RevolutTest
//
//  Created by Yuriy Durnev on 03.09.17.
//  Copyright © 2017 Revolut. All rights reserved.
//

import Foundation
import RealmSwift

class HistoryModel: Object {
    @objc dynamic var id: String?
    @objc dynamic var date: Date?
    var operation = List<ExchangeOperation>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

class ExchangeOperation: Object {
    @objc dynamic var id: String?
    @objc dynamic var date: Date?
    @objc dynamic var descriptionExchange: String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
