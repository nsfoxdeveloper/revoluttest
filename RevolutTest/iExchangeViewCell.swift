//
//  iExchangeViewCell.swift
//  RevolutTest
//
//  Created by Yuriy Durnev on 03.09.17.
//  Copyright © 2017 Revolut. All rights reserved.
//

import UIKit

class iExchangeViewCell: UIView {
    
    @IBOutlet weak var currencyName: UILabel!
    @IBOutlet weak var currencyValue: UILabel!
    @IBOutlet weak var textInput: UITextField!
    @IBOutlet weak var currencyRate: UILabel!
    
    public var cellTextChange: ((_ text: String?) -> Void)?
    private let worker = ExchangeWorker()
    
    var currency: UserCurrency? {
        didSet {
            guard let currency = currency,
                let name = currency.name,
                let symbol = currency.symbol else {
                    return
            }
            currencyName.text = name
            currencyValue.attributedText = worker.getString(value: currency.value, symbol: symbol, bigFontSize: 20, size: 16)
            currencyRate.textColor = .white
            textInput?.textColor = currencyRate.textColor
            currencyName.textColor = currencyRate.textColor
            currencyValue.textColor = currencyRate.textColor
        }
    }
    
    //Конвертация из строки ввода в строку другой ячейки
    func convert(curreny: UserCurrency, amout: Double) {
        let currencyStore = CurrencyDataStorage()
        
        guard let currentCurency = self.currency,
            let convertRate = currencyStore.getCurrencyBy(userCurrency: curreny),
            let currentRate = currencyStore.getCurrencyBy(userCurrency: currentCurency)
        else {
            if let convertRate = currencyStore.getCurrencyBy(userCurrency: self.currency!) {
                self.textInput.text = worker.currencyFormater(code: self.currency!.name, String(convertRate.rate * amout))
            } else if let convertRate = currencyStore.getCurrencyBy(userCurrency: curreny) {
                self.textInput.text = worker.currencyFormater(code: self.currency!.name, String(1 / convertRate.rate * amout))
            }
            return
        }
        
        if let name = curreny.name, name == "EUR" {
            let formater = worker.currencyFormater(code: self.currency!.name, String(currentRate.rate * amout))
            self.textInput.text = formater
        } else {
            let value = currentRate.rate / convertRate.rate * amout
            let formater = worker.currencyFormater(code: self.currency!.name, String(value))
            self.textInput.text = formater
        }
    }
    
    //Показываем соотношение 1:1
    func currency(currency: UserCurrency) {
        let currencyStore = CurrencyDataStorage()
        guard let currentCurency = self.currency,
            let currentRate = currencyStore.getCurrencyBy(userCurrency: currentCurency)
            else {
                if let eur = self.currency, let convertRate = currencyStore.getCurrencyBy(userCurrency: currency) {
                  self.currencyRate.text = "\(eur.symbol!)1 = \(currency.symbol!)\(Float(convertRate.rate))"
                }
                return
        }
        
        if let name = currency.name, name == "EUR" {
            self.currencyRate.text = "\(currentCurency.symbol!)1 = \(currency.symbol!)\(Float(1.0/currentRate.rate))"
        } else {
            guard let convertRate = currencyStore.getCurrencyBy(userCurrency: currency) else {return}
            self.currencyRate.text = "\(currentCurency.symbol!)1 = \(currency.symbol!)\(Float(convertRate.rate / currentRate.rate))"
        }
    }
    
    @IBAction func textChange(_ sender: UITextField) {
        guard let text = sender.text, text != "", let current = self.currency else {
            return
        }
    
        if text == "-" {
            NotificationCenter.default.post(name: UpdateButtonNotification, object: false)
            return
        }
        if text == "- " {
            sender.text = nil
            self.cellTextChange?(nil)
            NotificationCenter.default.post(name: UpdateButtonNotification, object: false)
            return
        }
        guard let cleanText = worker.clean(string: text) else {return}
        self.cellTextChange?(cleanText)
        sender.text = "- \(cleanText)"
        
        if let value = Double(cleanText), value > current.value  {
            self.currencyValue.textColor = .red
            NotificationCenter.default.post(name: UpdateButtonNotification, object: false)
        } else if let value = Float(cleanText), value == 0 {
            NotificationCenter.default.post(name: UpdateButtonNotification, object: false)
        } else {
            NotificationCenter.default.post(name: UpdateButtonNotification, object: true)
            self.currencyValue.textColor = .white
        }
    }
}
