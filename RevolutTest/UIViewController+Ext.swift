//
//  UIViewController+Ext.swift
//  RevolutTest
//
//  Created by Yuriy Durnev on 02.09.17.
//  Copyright © 2017 Revolut. All rights reserved.
//

import UIKit
extension UIViewController {
    
    @IBAction func dismissViewContriller(sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
