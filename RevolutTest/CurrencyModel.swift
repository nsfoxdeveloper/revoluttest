//
//  CurrencyModel.swift
//  RevolutTest
//
//  Created by Yuriy Durnev on 02.09.17.
//  Copyright © 2017 Revolut. All rights reserved.
//

import Foundation
import RealmSwift
import SWXMLHash

final class Currency: Object, XMLIndexerDeserializable {
    @objc dynamic var name: String?
    @objc dynamic var rate: Double = 0.0
    
    override static func primaryKey() -> String? {
        return "name"
    }
    
    static func deserialize(_ node: XMLIndexer) throws -> Currency {
        let currency = Currency()
        currency.name = node.value(ofAttribute: "currency")
        try currency.rate = node.value(ofAttribute: "rate")
        return currency
    }
    
}
