//
//  HeaderCurrencyCell.swift
//  RevolutTest
//
//  Created by Yuriy Durnev on 02.09.17.
//  Copyright © 2017 Revolut. All rights reserved.
//

import UIKit

class HeaderCurrencyCell: UICollectionViewCell {
    
    @IBOutlet weak var currencyName: UILabel!
    @IBOutlet weak var currencyValue: UILabel!
    
    var currency: UserCurrency? {
        didSet {
            guard let currency = currency,
                let name = currency.name,
                let symbol = currency.symbol else {
                return
            }
            currencyName.text = name
            currencyValue.attributedText = self.getString(value: currency.value, symbol: symbol, bigFontSize: 60, size: 40)
        }
    }
    
    func getString(value: Double, symbol: String, bigFontSize: CGFloat, size: CGFloat) -> NSMutableAttributedString {
        let intValue = String(Int(value))
        let string = symbol + String(value)
        let attr = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: bigFontSize)]
        let attributedString = NSMutableAttributedString(string: string, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: size)])
        attributedString.addAttributes(attr, range: NSRange(location: 1, length: intValue.characters.count))
        return attributedString
    }
}
