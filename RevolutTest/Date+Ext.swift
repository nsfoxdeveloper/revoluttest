//
//  Date+Ext.swift
//  RevolutTest
//
//  Created by Yuriy Durnev on 03.09.17.
//  Copyright © 2017 Revolut. All rights reserved.
//

import Foundation

extension Date {
    func toString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = NSTimeZone.local
        return dateFormatter.string(from: self)
    }
}
