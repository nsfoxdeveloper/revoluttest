//
//  HistoryStorage.swift
//  RevolutTest
//
//  Created by Yuriy Durnev on 03.09.17.
//  Copyright © 2017 Revolut. All rights reserved.
//

import Foundation
import RealmSwift

protocol HistoryProtocol {
    associatedtype T
    func getAll() -> Results<HistoryModel>
    func save(operationDescription: String)
}

class HistoryStorage: HistoryProtocol {
    typealias T = Results<HistoryModel>
    
    func getAll() -> Results<HistoryModel> {
        let realm = try! Realm()
        return realm.objects(HistoryModel.self).sorted(byKeyPath: "date", ascending: false)
    }
    
    //Чтобы не дергать Realm где попало
    func update(block: (() -> Void)) {
        let realm = try! Realm()
        try! realm.write {
            block()
        }
    }
    
    func save(operationDescription: String) {
        let realm = try! Realm()
        let newOperation = ExchangeOperation()
        newOperation.id = UUID().uuidString
        newOperation.descriptionExchange = operationDescription
        
        if let existTodayAllOpration = realm.object(ofType: HistoryModel.self, forPrimaryKey: Date().toString()) {
            try! realm.write {
                existTodayAllOpration.operation.insert(newOperation, at: 0)
                realm.add(existTodayAllOpration, update: true)
            }
        } else {
            let newDayList = HistoryModel()
            let date = Date()
            newDayList.id = date.toString()
            newDayList.date = date
            newDayList.operation.append(newOperation)
            
            try! realm.write {
                realm.add(newDayList)
            }
        }
    }
}
