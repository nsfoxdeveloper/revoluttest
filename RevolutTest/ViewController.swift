//
//  ViewController.swift
//  RevolutTest
//
//  Created by Yuriy Durnev on 02.09.17.
//  Copyright © 2017 Revolut. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var day = 1
    
    private var notificationToken: NotificationToken? = nil
    let operations = HistoryStorage().getAll()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = User().getUser().name
        
        notificationToken = operations.addNotificationBlock { [weak self] (changes: RealmCollectionChange) in
            guard let tableView = self?.tableView else { return }
            switch changes {
            case .initial:
                tableView.reloadData()
                break
            case .update(_,  _,  _,  _):
                tableView.reloadData()
                break
            case .error(let error):
                debugPrint(error.localizedDescription)
                break
            }
        }
    }
    
    deinit {
        notificationToken?.stop()
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return operations.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return operations[section].operation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = operations[indexPath.section].operation[indexPath.row].descriptionExchange
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return operations[section].id
    }
}

