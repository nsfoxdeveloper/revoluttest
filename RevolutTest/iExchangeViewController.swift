//
//  iExchangeViewController.swift
//  RevolutTest
//
//  Created by Yuriy Durnev on 03.09.17.
//  Copyright © 2017 Revolut. All rights reserved.
//

import UIKit

let UpdateButtonNotification = NSNotification.Name(rawValue: "UpdateButton")

class iExchangeViewController: UIViewController {
    
    @IBOutlet weak var stackViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var navigationView: UIView! {
        didSet {
            navigationView.layer.cornerRadius = 6
            navigationView.layer.borderColor = UIColor.white.cgColor
            navigationView.layer.borderWidth = 0.5
            navigationView.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var currencyTitleLabel: UILabel!
    @IBOutlet weak var topView: CarouselExchangeView!
    @IBOutlet weak var bottomView: CarouselExchangeView!
    @IBOutlet weak var exchangeButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        exchangeButton.isEnabled = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.disableExchange(notification:)), name: UpdateButtonNotification, object: nil)
        
        topView.cellBlockVisible = { [weak self] cell, index in
            guard let strongSelf = self else { return }
            cell.cellTextChange = { [weak self] text in
                guard let strongSelf = self, let text = text, let amout = Double(text) else {
                    self?.bottomView.visibleCell?.textInput.text = nil
                    return
                }
                strongSelf.bottomView.visibleCell?.convert(curreny: cell.currency!, amout: amout)
            }
            
            if let bottomCurrency = strongSelf.bottomView.visibleCell?.currency {
                if bottomCurrency == cell.currency! {
                    strongSelf.bottomView.nextItem()
                }
            }
            strongSelf.bottomView.visibleCell?.currency(currency: cell.currency!)
            cell.textInput.becomeFirstResponder()
        }
        
        bottomView.cellBlockVisible = { [weak self] cell, index in
            guard let strongSelf = self, let topVisibleCell = strongSelf.topView.visibleCell else {
                return
            }
            
            if let topCurrency = topVisibleCell.currency {
                if topCurrency == cell.currency! {
                    strongSelf.bottomView.nextItem()
                }
                strongSelf.bottomView.visibleCell?.currency(currency: topCurrency)
                if let text = ExchangeWorker().clean(string: topVisibleCell.textInput.text), let amout = Double(text) {
                     strongSelf.bottomView.visibleCell?.convert(curreny: topCurrency, amout: amout)
                } else {
                    strongSelf.bottomView.visibleCell?.textInput.text = nil
                }
            }
            strongSelf.topView.visibleCell?.currency(currency: cell.currency!)
            strongSelf.currencyTitleLabel.text = topVisibleCell.currencyRate.text
            strongSelf.topView.visibleCell?.currencyRate.text = " "
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.topView.visibleCell?.textInput.becomeFirstResponder()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func disableExchange(notification: NSNotification) {
        if let value = notification.object as? Bool {
            self.exchangeButton.isEnabled = value
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.stackView.removeFromSuperview()
    }
    
    @IBAction func exchange(_ sender: Any) {
        let worker = ExchangeWorker()
        
        if let valueTop = worker.clean(string: self.topView.visibleCell?.textInput.text),
            let valueBot = worker.clean(string: self.bottomView.visibleCell?.textInput.text),
            let userCurrencyTop = self.topView.visibleCell?.currency,
            let userCurrencyBot = self.bottomView.visibleCell?.currency {
            let history = HistoryStorage()
            
            history.update {
                userCurrencyTop.value = userCurrencyTop.value - Double(valueTop)!
                userCurrencyBot.value = userCurrencyBot.value + Double(valueBot)!
            }
            let operDescription = "Exchage \(userCurrencyTop.symbol!)\(valueTop) to \(userCurrencyBot.symbol!)\(valueBot)"
            history.save(operationDescription: operDescription)
            UIAlertExtention().show(title: "Success", message: operDescription, showOn: self)
            self.topView.visibleCell?.textInput.text = nil
            self.bottomView.visibleCell?.textInput.text = nil
        }
    }
}
