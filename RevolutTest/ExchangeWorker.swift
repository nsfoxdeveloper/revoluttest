//
//  ExchangeWorker.swift
//  RevolutTest
//
//  Created by Yuriy Durnev on 08.10.2017.
//  Copyright © 2017 Revolut. All rights reserved.
//

import Foundation
import UIKit

struct ExchangeWorker {
    
    func getString(value: Double, symbol: String, bigFontSize: CGFloat, size: CGFloat) -> NSMutableAttributedString {
        let intValue = String(Int(value))
        let string = symbol + String(value)
        let attr = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: bigFontSize)]
        let attributedString = NSMutableAttributedString(string: string, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: size)])
        attributedString.addAttributes(attr, range: NSRange(location: 1, length: intValue.characters.count))
        
        let youHave = NSAttributedString(string: "You have ", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16)])
        let final = NSMutableAttributedString()
        final.append(youHave)
        final.append(attributedString)
        return final
    }
    
    func currencyFormater(code: String?, prefix: String = "+ ", _ value: String?) -> String {
        guard let value = value else { return "0.00" }
        guard let doubleValue = Double(value) else { return "0.00" }
        let formatter = NumberFormatter()
        if let code = code {
            formatter.currencyCode = code
        }
        formatter.currencySymbol = ""
        formatter.positivePrefix = prefix
        formatter.currencyDecimalSeparator = "."
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        formatter.numberStyle = .currencyAccounting
        return formatter.string(from: NSNumber(value: doubleValue))!
    }
    
    func clean(string: String?) -> String? {
        return string?.replacingOccurrences(of: "[^0-9.,]", with: "", options: .regularExpression, range: nil)
            .replacingOccurrences(of: ",", with: ".")
            .trimmingCharacters(in: .whitespacesAndNewlines)
    }
}
