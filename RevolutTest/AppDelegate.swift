//
//  AppDelegate.swift
//  RevolutTest
//
//  Created by Yuriy Durnev on 02.09.17.
//  Copyright © 2017 Revolut. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let service = CurrencyService()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        UINavigationBar.appearance().tintColor = .white
        
        //Запиливаем тестового юзера и накидываем ему кеша
        User().createUser()
        return true
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        service.stopTimer()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        //Дергаем список валют и обновляем его
        service.startUpdate()
        service.sendRequest()
    }

}

